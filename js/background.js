(function () {
    function QueryString(url) {
        if (!(this instanceof QueryString)) {
            return new QueryString(url);
        }
        this.url = url;
        this.param = {};
    }

    QueryString.prototype.parse = function () {
        var that = this;
        var query = this.url.substring(this.url.indexOf('?') + 1);
        var values = query.split('&');

        _.forEach(values, function (value) {
            var pair = value.split('=');
            var k = pair[0];
            that.param[k] = (pair[1] == (void 0) ? '' : pair[1]);
        });
        return this.param;
    };

    QueryString.prototype.get = function (key) {
        return this.param[key];
    };

    function Crawler(url) {
        if (!(this instanceof Crawler)) {
            return new Crawler(url);
        }
        this.url = url;
        this.html = null;
    }

    Crawler.prototype.crawl = function (callback) {
        var that = this;
        $.get(this.url, null, function (data, status) {
            if (_.isEqual("success", status)) {
                that.html = data;
                callback(data);

            } else {
                throw "Crawl exception."
            }
        });
        return this;
    };

    function updateNewDealBadgeCount (html) {
        var newDeals = [];
        $('<html />').html(html).find("a.deal").each(function (i, e) {
            newDeals.push(new QueryString(e.href).parse()["coupang"]);
        });

        var badgeText = "";
        if(_.isUndefined(localStorage.deals)) {
            localStorage.setItem("deals", newDeals);
            badgeText = newDeals.length + "";

        } else {
            var oldDeals = localStorage.deals.split(",");
            var diff = _.difference(oldDeals, newDeals);
            if(diff.length > 0) {
                badgeText = diff.length + "";
            }
        }
        chrome.browserAction.setBadgeText({text: badgeText + ""});
    }

    function checkNewDeal() {
        var crawler = new Crawler("http://m.coupang.com/m/main.pang?type=newCoupang");
        crawler.crawl(updateNewDealBadgeCount)
    }

    checkNewDeal();

    (function loop() {
        setTimeout(function () {
            checkNewDeal();
            loop();
        }, 1000 * 60 * 60);
    })();

})();