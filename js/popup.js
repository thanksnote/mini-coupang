(function () {
    document.addEventListener('DOMContentLoaded', function () {

        var frameUrl = 'http://m.coupang.com/m/main.pang?client_agent=chrome_extension';
        chrome.browserAction.getBadgeText({}, function(result) {
            if(!_.isEmpty(result)) {
                frameUrl = 'http://m.coupang.com/m/main.pang?type=newCoupang';
            }
            chrome.browserAction.setBadgeText({text: ""});

            var frame = document.getElementById("miniCoupangFrame");
            frame.setAttribute("src", frameUrl);
            frame.setAttribute("width", 400);
            frame.setAttribute("height", 600);
            frame.setAttribute("frameborder", 0);
            frame.setAttribute("scrolling", 'yes');
            frame.blur();
        });


    });
})();